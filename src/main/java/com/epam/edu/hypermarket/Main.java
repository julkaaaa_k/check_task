package com.epam.edu.hypermarket;

import com.epam.edu.hypermarket.view.View;

public final class Main {

  private Main() {
  }

  public static void main(final String[] args) {
    new View().show();
  }
}
