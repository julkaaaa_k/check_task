package com.epam.edu.hypermarket.controller;

import com.epam.edu.hypermarket.model.Product;
import com.epam.edu.hypermarket.model.SubCategory;
import java.util.List;

public interface Controllable {
  List<Product> getProductsForTypicalRepair();
  List<Product> getProductsCheaperThan(SubCategory subCategory, double price);
  List<Product> getProductsFromCategory(SubCategory subCategory);
}
