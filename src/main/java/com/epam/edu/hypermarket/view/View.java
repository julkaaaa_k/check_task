package com.epam.edu.hypermarket.view;

import com.epam.edu.hypermarket.controller.Controller;
import com.epam.edu.hypermarket.model.Storage;
import com.epam.edu.hypermarket.model.SubCategory;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class View {

  private Controller controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in, "UTF-8");

  public View() {
    controller = new Controller(new Storage());
    menu = new LinkedHashMap<>();
    menu.put("1", "1 - getProductsForTypicalRepair");
    menu.put("2", "2 - getProductsCheaperThan");
    menu.put("3", "3 - getProductsFromCategory");
    menu.put("Q", "Q - exit");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::pressButton1);
    methodsMenu.put("2", this::pressButton2);
    methodsMenu.put("3", this::pressButton3);
  }


  private void printList(List list) {
    for (Object o : list) {
      System.out.println(o);
    }
  }

  private SubCategory subCategoryFromString(String stringSubCategory) {
    SubCategory subCategory = SubCategory.OTHER;
    try {
      subCategory = SubCategory.valueOf(stringSubCategory.toUpperCase().trim());
    } catch (IllegalArgumentException e) {
      System.out.println("No such category. Setting to OTHER");
    }
    return subCategory;
  }

  private void pressButton1() {
    printList(controller.getProductsForTypicalRepair());
  }

  /*варто зробити exception для випадків якщо такої категороії немає,
      або немає товарів нижче заданої ціни
      І мені здається, було б цікавіше, якби можна було шукати нижче заданої ціни
      серед усіх товарів, а не лише за однією категорією
   */

  /*  загалом код зрозумілий і все супер */

  private void pressButton2() {
    System.out.println("Enter product category: ");
    SubCategory subCategory = subCategoryFromString(input.nextLine());
    System.out.println("Enter upper price level: ");
    double price = input.nextDouble();
    printList(controller.getProductsCheaperThan(subCategory, price));
  }


  private void pressButton3() {
    System.out.println("Enter product category: ");
    SubCategory subCategory = subCategoryFromString(input.nextLine());
    printList(controller.getProductsFromCategory(subCategory));
  }

  private void outputMenu() {
    System.out.println("\nMenu:");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please, select menu point.");
      keyMenu = input.nextLine().toLowerCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
        System.out.println("Wrong input.");
      }
    } while (!keyMenu.equalsIgnoreCase("q"));
  }
}

