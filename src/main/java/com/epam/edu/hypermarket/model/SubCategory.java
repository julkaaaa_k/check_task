package com.epam.edu.hypermarket.model;

public enum SubCategory {
  WASHBASIN(Category.PLUMBING),
  BOWL(Category.PLUMBING),
  DOOR(Category.WOODEN_PRODUCTS),
  FLOOR(Category.WOODEN_PRODUCTS),
  LAMINATE(Category.WOODEN_PRODUCTS),
  PAINT(Category.PAINTS_AND_VARNISHES),
  VARNISH(Category.PAINTS_AND_VARNISHES),
  OTHER(Category.OTHER);

  SubCategory(Category cat) {
  }

  enum Category {
    PLUMBING,
    WOODEN_PRODUCTS,
    PAINTS_AND_VARNISHES,
    OTHER

  }


}
