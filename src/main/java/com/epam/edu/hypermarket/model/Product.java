package com.epam.edu.hypermarket.model;

public class Product {

  private double amount;
  private double price;
  private String name;
  private SubCategory subCategory;

  public Product(double amount, double price, String name,
      SubCategory subCategory) {
    this.amount = amount;
    this.price = price;
    this.name = name;
    this.subCategory = subCategory;
  }

  public double getAmount() {
    return amount;
  }

  public void setAmount(double amount) {
    this.amount = amount;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public SubCategory getSubCategory() {
    return subCategory;
  }

  public void setSubCategory(SubCategory subCategory) {
    this.subCategory = subCategory;
  }

  @Override
  public String toString() {
    return "Product{"
        + "amount="
        + amount
        + ", price="
        + price
        + ", name='"
        + name
        + '\''
        + ", subCategory="
        + subCategory
        + '}';
  }
}
