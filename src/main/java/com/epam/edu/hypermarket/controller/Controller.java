package com.epam.edu.hypermarket.controller;

import com.epam.edu.hypermarket.model.Product;
import com.epam.edu.hypermarket.model.Storage;
import com.epam.edu.hypermarket.model.SubCategory;
import java.util.ArrayList;
import java.util.List;

public class Controller implements Controllable {

  private Storage storage;

  public Controller(final Storage s) {
    this.storage = s;
  }

  @Override
  public final List<Product> getProductsForTypicalRepair() {
    return storage.getList();
  }

  @Override
  public final List<Product> getProductsCheaperThan(
      final SubCategory subCategory,
      final double price) {
    List<Product> requestedProducts = new ArrayList<>();
    for (Product p : getProductsFromCategory(subCategory)) {
      if (p.getPrice() < price) {
        requestedProducts.add(p);
      }
    }
    return requestedProducts;
  }

  @Override
  public final List<Product> getProductsFromCategory(
      final SubCategory subCategory) {
    List<Product> requestedProducts = new ArrayList<>();
    for (Product p : storage.getList()) {
      if (p.getSubCategory().equals(subCategory)) {
        requestedProducts.add(p);
      }
    }
    return requestedProducts;
  }
}
