package com.epam.edu.hypermarket.view;

@FunctionalInterface
public interface Printable {
  void print();
}
